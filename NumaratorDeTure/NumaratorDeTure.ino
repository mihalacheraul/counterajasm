/*********
  Rui Santos
  Complete project details at https://randomnerdtutorials.com/esp8266-nodemcu-access-point-ap-web-server/

  Permission is hereby granted, free of charge, to any person obtaining a copy
  of this software and associated documentation files.
  The above copyright notice and this permission notice shall be included in all
  copies or substantial portions of the Software.
*********/

// Import required libraries
#include <SPI.h>
#include <MFRC522.h>
#include <EEPROM.h>


#define RST_PIN              9          // Configurable, see typical pin layout above
#define SS_PIN               10         // Configurable, see typical pin layout above
#define LED                  3
#define Buzzer               5

MFRC522 mfrc522(SS_PIN, RST_PIN);   // Create MFRC522 instance

// Generally, you should use "unsigned long" for variables that hold time
// The value will quickly become too large for an int to store
unsigned long Time;
unsigned long lastTime[20] = {0};
int punches[20] = {0};
unsigned long actualTime;


/*!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!*/
/**************************************** T I M P   I N T R E    C I T I R I********************************/
int perioadaIntreCitiri = 10;  //secunde
/*-----------------------------------------------------------------------------------------------------------*/
/*-----------------------------------------------------------------------------------------------------------*/

void setup() {

  Serial.begin(9600);
  pinMode(2, OUTPUT);
  pinMode(3, OUTPUT);
  pinMode(4, OUTPUT);
  pinMode(5, OUTPUT);
  pinMode(6, OUTPUT);
  pinMode(7, OUTPUT);
  pinMode(8, OUTPUT);
  digitalWrite(2, LOW);
  digitalWrite(4, LOW);
  digitalWrite(7, LOW);
  digitalWrite(5, LOW);
  SPI.begin();                                                  // Init SPI bus
  mfrc522.PCD_Init();
  for (int i = 0; i < 20; i++)    //initialize
  {
    lastTime[i] = 0;
    punches[i] = (int)(char(EEPROM.read(i)));
  }
  analogWrite(3, 8);
  analogWrite(6, 8);
}

void loop() {

  if ( ! mfrc522.PICC_IsNewCardPresent())
  {
    return;
  }
  if ( ! mfrc522.PICC_ReadCardSerial())
  {
    return;
  }
  String content = "";
  content = readRFIDTag();
  //**********************************TAG 1*******************************************************//
  if (content.substring(1) == "29 D0 D2 B1")
  {
    callTag(1);
  }
  //**********************************TAG 2*******************************************************//
  if (content.substring(1) == "19 ED C9 B1")
  {
    callTag(2);
  }
  //**********************************TAG 3*******************************************************//
  if (content.substring(1) == "BB B9 C6 02")
  {
    callTag(3);
  }
  //**********************************TAG 4*******************************************************//
  if (content.substring(1) == "69 35 C7 B0")
  {
    callTag(4);
  }
  //**********************************TAG 5*******************************************************//
  if (content.substring(1) == "19 33 AF B0")
  {
    callTag(5);
  }
  //**********************************TAG 6*******************************************************//
  if (content.substring(1) == "79 39 D5 B1")
  {
    callTag(6);
  }
  //**********************************TAG 7*******************************************************//
  if (content.substring(1) == "D9 E6 C5 B0")
  {
    callTag(7);
  }
  //**********************************TAG 8*******************************************************//
  if (content.substring(1) == "9D B8 C6 02")
  {
    callTag(8);
  }
  //**********************************TAG 9*******************************************************//
  if (content.substring(1) == "49 93 CC B0")
  {
    callTag(9);
  }
  //**********************************TAG 10*******************************************************//
  if (content.substring(1) == "A9 33 D3 B1")
  {
    callTag(10);
  }

  //**********************************TAG 11*******************************************************//
  if (content.substring(1) == "09 06 B8 B0")
  {
    callTag(11);
  }
  //**********************************TAG 12*******************************************************//
  if (content.substring(1) == "59 F1 CB B1")
  {
    callTag(12);
  }
  //**********************************TAG 13*******************************************************//
  if (content.substring(1) == "07 82 C7 02")
  {
    callTag(13);
  }
  //**********************************TAG 14*******************************************************//
  if (content.substring(1) == "09 E0 BC B0")
  {
    callTag(14);
  }
  //**********************************TAG 15*******************************************************//
  if (content.substring(1) == "34 A5 C6 02")
  {
    callTag(15);
  }
  //**********************************TAG 16*******************************************************//
  if (content.substring(1) == "C2 42 C7 02")
  {
    callTag(16);
  }
  //**********************************TAG 17*******************************************************//
  if (content.substring(1) == "29 34 D3 B1")
  {
    callTag(17);
  }
  //**********************************TAG 18*******************************************************//
  if (content.substring(1) == "D9 B2 05 C2")
  {
    callTag(18);
  }
  //**********************************TAG 19*******************************************************//
  if (content.substring(1) == "A9 68 BC B1")
  {
    callTag(19);
  }
  //**********************************TAG 20*******************************************************//
  if (content.substring(1) == "D1 90 C7 02")
  {
    callTag(20);
  }

  //-------------------------------MASTER TAG SHOW DATA-------------------------------------------
  if (content.substring(1) == "C9 CC 72 9C")
  {
    SignalOn();
    Serial.println();
    Serial.print("Rezultatele actuale: ");
    for (int i = 0; i < 9; i++)
    {
      if (punches[i] != 0)
      {
        Serial.println();
        Serial.print("Tag");
        Serial.print(i + 1);
        Serial.print(" ----> ");
        Serial.print(punches[i]);
        Serial.print(" ture");
      }
    }
    for (int i = 10; i < 20; i++)
    {
      if (punches[i] != 0)
      {
        Serial.println();
        Serial.print("Tag");
        Serial.print(i + 1);
        Serial.print(" ---> ");
        Serial.print(punches[i]);
        Serial.print(" ture");
      }
    }
    delay(750);
  }
  //-------------------------------MASTER TAG DELETE-------------------------------------------
  if (content.substring(1) == "E9 9E BF B1")
  {
    SignalOn();
    Serial.println();
    Serial.print("Rezultatele actuale: ");
    for (int i = 0; i < 9; i++)
    {
      if (punches[i] != 0)
      {
        Serial.println();
        Serial.print("Tag");
        Serial.print(i + 1);
        Serial.print(" ----> ");
        Serial.print(punches[i]);
        Serial.print(" ture");
      }
    }
    for (int i = 10; i < 20; i++)
    {
      if (punches[i] != 0)
      {
        Serial.println();
        Serial.print("Tag");
        Serial.print(i + 1);
        Serial.print(" ---> ");
        Serial.print(punches[i]);
        Serial.print(" ture");
      }
    }
    Serial.println();
    for (int i = 0; i < 20; i++)
    {
      punches[i] = 0;
      EEPROM.write(i, 0);
    }
    Serial.println("REZULTATE RESETATE!");
  }
}

void callTag(int i)
{
  if (actualTime - lastTime[i - 1] >= perioadaIntreCitiri)
  {
    punches[i - 1] = (int)(char(EEPROM.read(i - 1)));
    punches[i - 1]++;
    lastTime[i - 1] = actualTime;
    Serial.print(punches[i - 1]);
    Serial.println(" ture");
    EEPROM.write(i - 1, punches[i - 1]);
    SignalOn();
  }
}

void SignalOn()
{
  digitalWrite(3, HIGH);
  digitalWrite(6, HIGH);
  digitalWrite(Buzzer, HIGH);
  delay(150);
  analogWrite(3, 8);
  analogWrite(6, 8);
  digitalWrite(Buzzer, LOW);
}


String readRFIDTag()
{
  String content = "";
  for (byte i = 0; i < mfrc522.uid.size; i++)
  {
    content.concat(String(mfrc522.uid.uidByte[i] < 0x10 ? " 0" : " "));
    content.concat(String(mfrc522.uid.uidByte[i], HEX));
  }
  actualTime = millis() / 1000;
  content.toUpperCase();
  return content;
}
